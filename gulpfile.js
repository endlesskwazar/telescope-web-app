var gulp = require('gulp');
var browserSynv = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var cleanCss = require('gulp-clean-css');
var sass = require('gulp-sass');


gulp.task('html', function(){
    gulp.src('src/index.html')
    .pipe(gulp.dest('build/'))
    .pipe(browserSynv.stream());
});

gulp.task('css', function(){
    gulp.src('src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['cover 99.5%']
    }))
    .pipe(cleanCss())
    .pipe(gulp.dest('build/css'))
    .pipe(browserSynv.stream());
});

gulp.task('img', function(){
    gulp.src('src/img/**/*')
    .pipe(gulp.dest('build/img'))
    .pipe(browserSynv.stream());
});

gulp.task('serve', ['html', 'css', 'img'], function(){
    browserSynv.init({
        server: 'build'
    });

    gulp.watch('src/index.html', ['html']);
    gulp.watch('src/scss/style.scss', ['css']);
    gulp.watch('src/img/**/*', ['img']);
});

gulp.task('default', ['serve']);